FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-buster AS build
WORKDIR /src
COPY ["CommunicationService/ShowFam.Communication.csproj", "CommunicationService/"]
COPY ["ShowFam.Common/ShowFam.Common.csproj", "ShowFam.Common/"]
COPY ["gapi_auth.json", "CommunicationService/"]
RUN dotnet restore "CommunicationService/ShowFam.Communication.csproj"
COPY . .
WORKDIR "/src/CommunicationService"
RUN dotnet build "ShowFam.Communication.csproj" -c Debug -o /app
COPY ["gapi_auth.json","/app"]
FROM build AS publish
RUN dotnet publish "ShowFam.Communication.csproj" -c Debug -o /app

FROM base AS final
ENV GOOGLE_APPLICATION_CREDENTIALS=/app/gapi_auth.json
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "ShowFam.Communication.dll"]
