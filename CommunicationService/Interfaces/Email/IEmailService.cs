﻿using CommunicationService.Models;
using Showfam.Common.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationService.Interfaces.Email
{
    public interface IEmailService
    {
        void SendEmail(EmailRecipient from, EmailRecipient to, EmailMessage message, IList<EmailRecipient> cc = null, IList<EmailRecipient> bcc = null);
    }
}
