﻿using Showfam.Common.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationService.Interfaces.Sms
{
    public interface ISmsService
    {
        void SendSms(SmsMessage message, IList<SmsRecipient> recipients, SmsRecipient from);
    }
}
