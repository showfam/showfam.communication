﻿using System.Runtime.InteropServices;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace CommunicationService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

       

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureKestrel(options =>
                {
                    options.Limits.MinRequestBodyDataRate = null;
                    options.ListenAnyIP(80, listenOptions =>
                    {
                        /*
                        // ALPN is not available on macOS so only use Https on Windows and Linux
                        if (!RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                        {
                            listenOptions.UseHttps(ResourceSet.ServerPFXPath, "1111");
                        }*/
                        listenOptions.Protocols = HttpProtocols.Http2;
                    });
                })
                .UseStartup<Startup>();
    }
}
