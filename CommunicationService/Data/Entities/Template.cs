﻿using Google.Cloud.Firestore;
using ShowFam.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShowFam.Communication.Data.Entities
{
    [FirestoreData]
    public class Template : BaseNoSqlEntity
    {
        /**
         * Service Owner is the service this template belongs to
         */
        [FirestoreProperty]
        public string ServiceOwner { set; get; }
        [FirestoreProperty]
        public string Name { set; get; }
        [FirestoreProperty]
        public string HtmlText { set; get; }
        [FirestoreProperty]
        public string PlainText { set; get; }
        [FirestoreProperty]
        public string SmsText { set; get; }
        [FirestoreProperty]
        public string Subject { set; get; }
    }
}
