﻿
using ShowFam.Common.Data.NoSqlRepositoryServices;
using ShowFam.Communication.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShowFam.Common.Data.ISqlRepositoryServices
{
    public interface ITemplateRepository: IBaseNoSqlRepositoryWithId<Template, string>
    {
        Template GetByName(string name, string serviceName);
        Template GetForService(string id, string serviceName);
    }
}
