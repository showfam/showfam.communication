﻿
using ShowFam.Common.Data.ISqlRepositoryServices;
using ShowFam.Common.Data.NoSqlRepositoryServices;
using ShowFam.Communication.Data.Entities;
using System.Linq;

namespace ShowFam.Users.Data.RepositoryServices
{
    public class TemplateRepository: BaseFireBaseRepositoryWithId<Template>, ITemplateRepository
    {
        public TemplateRepository(string projectId)
            : base(projectId, "communicationTemplates")
        {

        }

        public Template GetByName(string name, string serviceName)
        {
            return GetAllBy( new FireBaseQueryBuilder(CollectionReference()).EqualTo("Name", name).EqualTo("ServiceName", serviceName).Get()).FirstOrDefault();
        }

        public Template GetForService(string id, string serviceName)
        {
            var res = Get(id);
            return res != null && res.ServiceOwner == serviceName ? res : null;
        }
    }
}
