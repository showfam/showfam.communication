﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunicationService.Interfaces.Email;
using CommunicationService.Interfaces.Sms;
using CommunicationService.Models;
using CommunicationService.Services.Email;
using CommunicationService.Services.Sms;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShowFam.Common.Services;
using ShowFam.Common.Data.ISqlRepositoryServices;
using ShowFam.Communication.Data;
using ShowFam.Communication.Rpc;
using ShowFam.Communication.Store;
using ShowFam.Users.Data.RepositoryServices;
using Grpc.Net.ClientFactory;
using Showfam.Common.Settings;

namespace CommunicationService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddGrpc();

            var appSettings = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettings);

            services.AddTransient<ISmsService, TwilioService>();
            services.AddTransient<IEmailService, SendGridService>();
            services.AddTransient<TemplateModelStore, TemplateModelStore>();


            services.AddTransient<ITemplateRepository>(s => new TemplateRepository(Configuration["AppSettings:ProjectId"]));
            /*
            services.AddTransient<ISettingCache>(s =>
            {
                System.Console.WriteLine("starting setting service...");
                var sc = new SettingCache("settings");
                System.Console.WriteLine("setting service complete....");
                return sc;
            });
            */
            services.AddGrpcClient<SettingService.SettingServiceClient>(o =>
            {
                AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
                o.BaseAddress = new Uri("http://settings");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseHsts();
            }
            app.UseRouting();
            app.UseEndpoints(routes =>
            {
                routes.MapGrpcService<EmailRpc>();
                routes.MapGrpcService<SmsRpc>();
                routes.MapGrpcService<TemplateRpc>();
            });
        }
    }
}
