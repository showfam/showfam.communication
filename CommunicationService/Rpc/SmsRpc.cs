﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunicationService.Interfaces.Sms;
using CommunicationService.Models;
using Grpc.Core;
using Microsoft.Extensions.Options;
using Showfam.Common.Communication;
using Showfam.Common.Settings;
using ShowFam.Common.Services;

namespace ShowFam.Communication.Rpc
{
    public class SmsRpc: Showfam.Common.Communication.SmsService.SmsServiceBase
    {
        private AppSettings settings;
        private ISmsService smsService;
        private IDictionary<string, string> mSettings;

        public SmsRpc(ISmsService smsService, IOptions<AppSettings> options, SettingService.SettingServiceClient _client)
        {
            Console.WriteLine("starting sms service...");
            this.smsService = smsService;
            settings = options.Value;

            var settingCache = new SettingCache(_client);
            Console.WriteLine("starting sms service...ALMOST");
            mSettings = settingCache.FetchAllForServiceNew("communication");
            Console.WriteLine("starting sms service...DOOOOOOOOOOOONE");
        }

        public override async Task<SmsReplyModel> send(SendSmsModel request, ServerCallContext context)
        {
            return await Task.Run(() =>
            {
                Console.WriteLine("sms request received from: " + context.Host);
                var fromPhone = mSettings["DEFAULT_FROM_SMS_NUMBER"];
                this.smsService.SendSms(request.Message, request.To, new SmsRecipient { PhoneNumber = fromPhone });
                return new SmsReplyModel
                {
                    Code = "200",
                    Message = "success"
                };
            });
            
        }
    }
}
