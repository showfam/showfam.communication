﻿using Grpc.Core;
using Showfam.Common.Communication;
using Showfam.Common.Settings;
using Showfam.Rpc;
using ShowFam.Common.Services;
using ShowFam.Communication.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShowFam.Communication.Rpc
{
    public class TemplateRpc : Showfam.Common.Communication.TemplateService.TemplateServiceBase
    {
        private TemplateModelStore templateModelStore;
        private IDictionary<string, string> mSettings;
        public TemplateRpc(TemplateModelStore templateModelStore, SettingService.SettingServiceClient _client)
        {
            var settingCache = new SettingCache(_client);
            Console.WriteLine("starting TEMPLATE service...ALMOST");
            mSettings = settingCache.FetchAllForServiceNew("communication");
            Console.WriteLine("starting TEMPLATE service...DOOOOOOOOOOOONE");
            this.templateModelStore = templateModelStore;
        }

        public override Task<RpcResult> Create(CreateTemplateModel request, ServerCallContext context)
        {
            var res = templateModelStore.Create(request);
            return  Task.FromResult(res);
        }

        public override Task<RpcResult> Delete(DeleteTemplateModel request, ServerCallContext context)
        {
            var res = templateModelStore.Delete(request);

            return Task.FromResult(res);
        }

        public override Task<FetchTemplateResult> Fetch(FetchTemplateModel request, ServerCallContext context)
        {
            var res = templateModelStore.Fetch(request);

            return Task.FromResult(res);
        }

        public override Task<RpcResult> Update(TemplateModel request, ServerCallContext context)
        {
            var res = templateModelStore.Update(request);

            return Task.FromResult(res);
        }
    }
}
