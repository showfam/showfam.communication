﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunicationService.Interfaces.Email;
using Grpc.Core;
using Showfam.Common.Communication;
using Showfam.Rpc;

namespace ShowFam.Communication.Rpc
{
    public class EmailRpc: Showfam.Common.Communication.EmailService.EmailServiceBase
    {
        private IEmailService emailService;
        public EmailRpc(IEmailService emailService)
        {
            this.emailService = emailService;
        }

        public override async Task<RpcResult> send(SendEmailModel request, ServerCallContext context)
        {
            return await Task.Run(() =>
            {
                Console.WriteLine("Email request received from: " + context.Host);
                emailService.SendEmail(request.From, request.To, request.Message, request.Cc, request.Bcc);
                return new RpcResult
                {
                    IsSuccessful = true,
                    Message = "success"
                };
            });
            
        }
    }
}
