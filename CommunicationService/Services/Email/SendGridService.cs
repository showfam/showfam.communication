﻿using CommunicationService.Interfaces.Email;
using CommunicationService.Models;
using Grpc.Core;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using Showfam.Common.Communication;
using Showfam.Common.Settings;
using ShowFam.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationService.Services.Email
{
    public class SendGridService : IEmailService
    {
        private SendGridClient client;
        private SettingService.SettingServiceClient settingClient;
        private IDictionary<string, string> mSettings;
        public SendGridService(IOptions<AppSettings> appSettings, SettingService.SettingServiceClient _client)
        {
            var settingCache = new SettingCache(_client);
            mSettings = settingCache.FetchAllForServiceNew("communication");
            client = new SendGridClient(mSettings["SENDGRID_API_KEY"]);
        }
        public async void SendEmail(EmailRecipient from, EmailRecipient to, EmailMessage message, IList<EmailRecipient> cc = null, IList<EmailRecipient> bcc = null)
        {
            var _from = new EmailAddress(from.Email, from.Name);
            var _to = new EmailAddress(to.Email, to.Name);

            var msg = MailHelper.CreateSingleEmail(_from, _to, message.Subject, message.PlainBody, message.HtmlBody);

            if (cc != null && cc.Count > 0)
            {
                var _ccs = cc.Select(x => new EmailAddress(x.Email, x.Name)).ToList();
                msg.AddCcs(_ccs);
            }
            if (bcc != null && bcc.Count > 0)
            {
                var _bccs = bcc.Select(x => new EmailAddress(x.Email, x.Name)).ToList();
                msg.AddBccs(_bccs);
            }
            var attachments = message.Attachments != null ? message.Attachments.ToList() : new List<EmailAttachment>();
            foreach(var item in attachments)
            {
                msg.AddAttachment(item.FileName, item.Content.ToBase64());
            }

            var response = await client.SendEmailAsync(msg);
        }
    }
}
