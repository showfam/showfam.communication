﻿using CommunicationService.Interfaces.Sms;
using CommunicationService.Models;
using Grpc.Core;
using Microsoft.Extensions.Options;
using Showfam.Common.Communication;
using Showfam.Common.Settings;
using ShowFam.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace CommunicationService.Services.Sms
{
    public class TwilioService : ISmsService
    {
        private SettingService.SettingServiceClient settingClient;
        private IDictionary<string, string> mSettings;
        public TwilioService(IOptions<AppSettings> options, SettingService.SettingServiceClient _client)
        {
            var channel = new Channel(options.Value.ServiceUrls["setting"], ChannelCredentials.Insecure);
            var settingCache = new SettingCache(_client);
            mSettings = settingCache.FetchAllForServiceNew("communication");

            var accountSid = mSettings["TWILIO_ACCOUNT_SID"];
            var authToken = mSettings["TWILIO_AUTH_TOKEN"];
            TwilioClient.Init(accountSid, authToken);
        }
        public void SendSms(SmsMessage message, IList<SmsRecipient> recipients, SmsRecipient from)
        {
            foreach(var item in recipients)
            {
                var msg = MessageResource.Create(
                    body: message.Message,
                    from: new Twilio.Types.PhoneNumber(from.PhoneNumber),
                    to: new Twilio.Types.PhoneNumber(item.PhoneNumber)
                    );
            }
        }
    }
}
