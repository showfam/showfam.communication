﻿using Showfam.Common.Communication;
using Showfam.Rpc;
using ShowFam.Common.Data.ISqlRepositoryServices;
using ShowFam.Common.Data.NoSqlRepositoryServices;
using ShowFam.Common.Utils;
using ShowFam.Communication.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShowFam.Communication.Store
{
    public class TemplateModelStore
    {
        private ITemplateRepository templateRepository;
        public TemplateModelStore(ITemplateRepository templateRepository)
        {
            this.templateRepository = templateRepository;
        }

        public RpcResult Create(CreateTemplateModel model)
        {
            if(string.IsNullOrEmpty(model.ServiceOwner) || string.IsNullOrEmpty(model.Name) ||
                string.IsNullOrEmpty(model.PlainText) ||string.IsNullOrEmpty(model.HtmlText))
            {
                return new RpcResult
                {
                    IsSuccessful = false,
                    Message = "Some fields are missing"
                };
            }
            if(templateRepository.GetByName(model.Name, model.ServiceOwner) != null)
            {
                return new RpcResult
                {
                    IsSuccessful = false,
                    Message = "Name is taken"
                };
            }
            var e = Copy(model);
            templateRepository.Save(e);
            return new RpcResult
            {
                Id = e.Id,
                IsSuccessful = true,
                Message = "success"
            };
        }
        public RpcResult Delete(DeleteTemplateModel model)
        {
            templateRepository.Delete(model.Id);
            return new RpcResult
            {
                IsSuccessful = true,
                Message = "template deleted"
            };
        }
        public RpcResult Update(TemplateModel model)
        {
            var e = this.templateRepository.Get(model.Id);
            if(e == null)
            {
                return new RpcResult
                {
                    IsSuccessful = false,
                    Message = "template with the id doesn't exist"
                };
            }

            bool needToUpdate = false;
            if(Validate.NeedToUpdate(e.HtmlText, model.HtmlText))
            {
                e.HtmlText = model.HtmlText;
                needToUpdate = true;
            }
            if (Validate.NeedToUpdate(e.PlainText, model.PlainText))
            {
                e.PlainText = model.PlainText;
                needToUpdate = true;
            }
            if (Validate.NeedToUpdate(e.SmsText, model.SmsText))
            {
                e.SmsText = model.SmsText;
                needToUpdate = true;
            }
            if (Validate.NeedToUpdate(e.Subject, model.Subject))
            {
                e.Subject = model.Subject;
                needToUpdate = true;
            }
            if (Validate.NeedToUpdate(e.Name, model.Name))
            {
                e.Name = model.Name;
                needToUpdate = true;
            }
            if (Validate.NeedToUpdate(e.ServiceOwner, model.ServiceOwner))
            {
                e.ServiceOwner = model.ServiceOwner;
                needToUpdate = true;
            }
            if (needToUpdate)
            {
                templateRepository.Update(e);
            }

            return new RpcResult
            {
                Id = e.Id,
                IsSuccessful = true,
                Message = needToUpdate ? "updated" : "template already up to date"
            };
        }
        public FetchTemplateResult Fetch(FetchTemplateModel model)
        {
            FetchTemplateResult res = new FetchTemplateResult();
            var colRef = templateRepository.CollectionReference();
            switch (model.FetchBy)
            {
                case FetchTemplateModel.Types.FetchCriteria.Id:
                    var item = templateRepository.GetForService(model.Id, model.ServiceOwner);
                    if(item != null)
                    {
                        res.Items.Add(Copy(item));
                    }
                    break;
                case FetchTemplateModel.Types.FetchCriteria.Name:
                    var items = templateRepository.GetAllBy(new FireBaseQueryBuilder(colRef).EqualTo("Name", model.Name).EqualTo("ServiceOwner", model.ServiceOwner).Get());
                    if(items != null)
                    {
                        foreach (var i in items)
                        {
                            if (i.ServiceOwner == model.ServiceOwner)
                            {
                                res.Items.Add(Copy(i));
                            }
                        }
                    }
                    break;
                case FetchTemplateModel.Types.FetchCriteria.ServiceOwner:
                    var mItems = templateRepository.GetAllBy(new FireBaseQueryBuilder(colRef).EqualTo("ServiceOwner", model.ServiceOwner).Get());
                    if(mItems != null)
                    {
                        var toAdd = mItems.Select(x => Copy(x));
                        res.Items.Add(toAdd);
                    }
                    break;
                case FetchTemplateModel.Types.FetchCriteria.All:
                    var allItems = templateRepository.GetAll();
                    if (allItems != null)
                    {
                        var toAdd = allItems.Select(x => Copy(x));
                        res.Items.Add(toAdd);
                    }
                    break;
            }

            return res;
        }
        private Template Copy(CreateTemplateModel model)
        {
            var e = templateRepository.CreateEntity();
            e.HtmlText = model.HtmlText;
            e.PlainText = model.PlainText;
            e.SmsText = model.SmsText;
            e.ServiceOwner = model.ServiceOwner;
            e.Name = model.Name;
            e.Subject = model.Subject;
            
            return e;
        }

        private TemplateModel Copy(Template model)
        {
            var e = new TemplateModel();
            e.Id = model.Id;
            e.HtmlText = model.HtmlText;
            e.PlainText = model.PlainText;
            e.SmsText = model.SmsText;
            e.ServiceOwner = model.ServiceOwner;
            e.Name = model.Name;
            e.Subject = model.Subject;

            return e;
        }
    }
}
