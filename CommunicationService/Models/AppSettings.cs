﻿using ShowFam.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationService.Models
{
    public class AppSettings
    {
        public string ProjectId { set; get; }
        public IDictionary<string,string> ServiceUrls { set; get; }
    }
}
